import 'package:weather_app/api/responses/weather_conditions_response.dart';
import 'package:weather_app/api/responses/weather_main_response.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherResponse {
  String? name;
  WeatherMainResponse? main;
  List<WeatherConditionsResponse> weather = [];
}
