import 'package:weather_app/api/responses/weather_conditions_response.dart';
import 'package:weather_app/helpers/format_date.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherCurrentResponse {
  num? dt;
  num? sunrise;
  num? sunset;
  num? temp;
  num? pressure;

  List<WeatherConditionsResponse> weather = [];

  @JsonProperty(ignore: true)
  DateTime? get date {
    return dateFromTimestamp(dt);
  }

  String get getMessage {
    if (temp == null) {
      return "No temp";
    } else if (temp! > 25) {
      return 'It\'s 🍦 time';
    } else if (temp! > 20) {
      return 'Time for shorts and 👕';
    } else if (temp! < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
