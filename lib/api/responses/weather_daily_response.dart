import 'package:weather_app/api/responses/weather_conditions_response.dart';
import 'package:weather_app/api/responses/weather_current_response.dart';
import 'package:weather_app/helpers/format_date.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherDailyResponse {
  WeatherCurrentResponse? current;
  List<DailyItemResponse> daily = [];
}

@jsonSerializable
class DailyItemResponse {
  num? dt;
  num? sunrise;
  num? sunset;
  num? pressure;

  DailyTempResponse? temp;

  List<WeatherConditionsResponse> weather = [];

  @JsonProperty(ignore: true)
  DateTime? get date {
    return dateFromTimestamp(dt);
  }

  @JsonProperty(ignore: true)
  DateTime? get sunriseDate {
    return dateFromTimestamp(sunrise);
  }

  @JsonProperty(ignore: true)
  DateTime? get sunsetDate {
    return dateFromTimestamp(sunset);
  }
}

@jsonSerializable
class DailyTempResponse {
  num? day;
  num? min;
  num? max;
  num? night;
}


// {
//             "dt": 1641837600,
//             "sunrise": 1641820844,
//             "sunset": 1641857177,
//             "moonrise": 1641839520,
//             "moonset": 1641797820,
//             "moon_phase": 0.28,
//             "temp": {
//                 "day": 277.73,
//                 "min": 274.26,
//                 "max": 283.77,
//                 "night": 277.29,
//                 "eve": 279.62,
//                 "morn": 274.33
//             },
//             "feels_like": {
//                 "day": 275.27,
//                 "night": 277.29,
//                 "eve": 279.62,
//                 "morn": 270.6
//             },
//             "pressure": 1039,
//             "humidity": 42,
//             "dew_point": 266.74,
//             "wind_speed": 5.42,
//             "wind_deg": 33,
//             "wind_gust": 11.77,
//             "weather": [
//                 {
//                     "id": 801,
//                     "main": "Clouds",
//                     "description": "few clouds",
//                     "icon": "02d"
//                 }
//             ],
//             "clouds": 19,
//             "pop": 0,
//             "uvi": 2.84
//         },