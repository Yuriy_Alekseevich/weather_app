import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherMainResponse {
  num? temp;
  num? pressure;

  String get getMessage {
    if (temp == null) {
      return "No temp";
    } else if (temp! > 25) {
      return 'It\'s 🍦 time';
    } else if (temp! > 20) {
      return 'Time for shorts and 👕';
    } else if (temp! < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }

  
}
