import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherConditionsResponse {
  num? id;
  String? main;
  String? description;

  String get getWeatherIcon {
    if (id == null) {
      return "🤷";
    }

    if (id! < 300) {
      return '🌩';
    } else if (id! < 400) {
      return '🌧';
    } else if (id! < 600) {
      return '☔️';
    } else if (id! < 700) {
      return '☃️';
    } else if (id! < 800) {
      return '🌫';
    } else if (id! == 800) {
      return '☀️';
    } else if (id! <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }
}
