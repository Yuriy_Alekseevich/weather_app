import 'package:weather_app/api/responses/weather_conditions_response.dart';
import 'package:weather_app/api/responses/weather_current_response.dart';
import 'package:weather_app/helpers/format_date.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class WeatherHourlyResponse {
  WeatherCurrentResponse? current;
  List<HourlyItemResponse> hourly = [];
}

@jsonSerializable
@Json(caseStyle: CaseStyle.Snake)
class HourlyItemResponse {
  num? dt;
  num? temp;
  num? pressure;
  num? windSpeed;

  List<WeatherConditionsResponse> weather = [];

  @JsonProperty(ignore: true)
  DateTime? get date {
    return dateFromTimestamp(dt);
  }
}
