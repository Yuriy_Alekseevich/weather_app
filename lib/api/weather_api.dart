import 'package:weather_app/api/responses/weather_daily_response.dart';
import 'package:weather_app/api/responses/weather_hourly_response.dart';
import 'package:weather_app/api/responses/weather_response.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:dio/dio.dart';
import 'package:geolocator/geolocator.dart';

const apiKey = '251f54e411d2a5be50dc02d9f778bcb7';
const openWeatherMapURL = 'https://api.openweathermap.org/data/2.5';

class WeatherApi {
  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: openWeatherMapURL,
      connectTimeout: 32000,
      receiveTimeout: 32000,
      headers: {
        Headers.contentTypeHeader: 'application/json',
        Headers.acceptHeader: 'application/json',
      },
    ),
  );

  Map<String, dynamic> _queryParameters({required Position position}) {
    return {'lat': position.latitude, 'lon': position.longitude, 'appid': apiKey, 'units': 'metric'};
  }

  Future<WeatherResponse?> fetchCurrentWeather({required Position position}) async {
    try {
      final response = await _dio.get<String>('/weather', queryParameters: _queryParameters(position: position));
      print(position.latitude);
      print(position.longitude);
      print(response.data);

      return JsonMapper.deserialize<WeatherResponse>(response.data);
    } catch (e) {
      throw e;
    }
  }

  Future<WeatherHourlyResponse?> fetchHourlyWeather({required Position position}) async {
    try {
      var params = _queryParameters(position: position);
      params.addAll({'exclude': 'minutely,daily'});

      final response = await _dio.get<String>('/onecall', queryParameters: params);

      return JsonMapper.deserialize<WeatherHourlyResponse>(response.data);
    } catch (e) {
      throw e;
    }
  }

  Future<WeatherDailyResponse?> fetchDailyWeather({required Position position}) async {
    try {
      var params = _queryParameters(position: position);
      params.addAll({'exclude': 'minutely,hourly'});

      final response = await _dio.get<String>('/onecall', queryParameters: params);

      return JsonMapper.deserialize<WeatherDailyResponse>(response.data);
    } catch (e) {
      throw e;
    }
  }
}
