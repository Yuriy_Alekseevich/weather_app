import 'dart:async';

import 'package:weather_app/blocs/weather_current_cubit.dart';
import 'package:weather_app/blocs/weather_daily_cubit.dart';
import 'package:weather_app/screens/daily_weather_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'current_weather_screen.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();

    Timer(
      Duration(seconds: 3),
      () {
        pushToLocationScreen();
      },
    );
  }

  void pushToDailyForecastScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BlocProvider(
          create: (context) => WeatherDailyCubit()..getData(),
          child: DailyWeatherScreen(),
        ),
      ),
    );
  }

  void pushToLocationScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BlocProvider(
          create: (context) => WeatherCurrentCubit()..getData(),
          child: CurrentWeatherScreen(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text('☔️', style: TextStyle(fontSize: 200)),
      ),
    );
  }
}
