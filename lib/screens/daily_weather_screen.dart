import 'package:weather_app/api/responses/weather_daily_response.dart';
import 'package:weather_app/blocs/base_states.dart';
import 'package:weather_app/blocs/weather_daily_cubit.dart';
import 'package:weather_app/helpers/format_date.dart';
import 'package:weather_app/screens/items/temperature_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DailyWeatherScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WeatherDailyCubit _cubit = BlocProvider.of<WeatherDailyCubit>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Daily Forecast',
          style: TextStyle(color: Colors.black, fontSize: 22),
        ),
        centerTitle: true,
        leading: Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/night_background.png'),
            fit: BoxFit.cover,
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: BlocBuilder(
          bloc: _cubit,
          builder: (context, state) {
            if (state is LoadedModelState<WeatherDailyResponse> && state.model.daily.isNotEmpty) {
              return Column(
                children: [
                  if (state.model.current != null)
                    Container(
                      child: Column(
                        children: [
                          Text("Current weather"),
                          SizedBox(height: 20),
                          Text("Current temp - ${state.model.current!.temp} - ${state.model.current!.getMessage}"),
                        ],
                      ),
                    ),
                  Expanded(
                    child: ListView.separated(
                      itemCount: state.model.daily.length,
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (context, index) {
                        final model = state.model.daily[index];
                        return InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("More info"),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TemperatureTextWidget(prefix: "Temp min - ", temp: model.temp?.min),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TemperatureTextWidget(prefix: "Temp max - ", temp: model.temp?.max),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TemperatureTextWidget(prefix: "Temp night - ", temp: model.temp?.night),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          model.pressure != null ? "Pressure - ${model.pressure}" : "No data",
                                          style: TextStyle(),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          model.sunriseDate != null
                                              ? "Sunrise - ${formatDate(model.sunriseDate, format: DateFormatType.Time)}"
                                              : "No data",
                                          style: TextStyle(),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          model.sunsetDate != null
                                              ? "Sunset - ${formatDate(model.sunsetDate, format: DateFormatType.Time)}"
                                              : "No data",
                                          style: TextStyle(),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Card(
                              color: Colors.white,
                              elevation: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      model.date != null ? formatDate(model.date, format: DateFormatType.Day) : "NaN",
                                      style: TextStyle(color: Colors.black),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TemperatureTextWidget(prefix: "Day -", temp: model.temp?.day),
                                  ),
                                  if (model.weather.isNotEmpty)
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        model.weather.first.main ?? "No name",
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  if (model.weather.isNotEmpty)
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        model.weather.first.getWeatherIcon,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            } else if (state is LoadedModelState<WeatherDailyResponse> && state.model.daily.isEmpty) {
              return Text(
                'Emply list',
                style: TextStyle(
                  color: Colors.grey[900],
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              );
            } else if (state is LoadingState) {
              return Center(
                child: Container(
                  height: 100,
                  width: 100,
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is ErrorState) {
              return Text(
                'Error - ${state.message}',
                style: TextStyle(
                  color: Colors.grey[900],
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              );
            }

            return Container();
          },
        ),
      ),
    );
  }
}
