import 'package:weather_app/api/responses/weather_response.dart';
import 'package:weather_app/blocs/weather_current_cubit.dart';
import 'package:weather_app/blocs/weather_daily_cubit.dart';
import 'package:weather_app/blocs/weather_hourly_cubit.dart';
import 'package:weather_app/screens/items/temperature_text_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/blocs/base_states.dart';

import 'daily_weather_screen.dart';
import 'hourly_weather_screen.dart';

class CurrentWeatherScreen extends StatefulWidget {
  @override
  _CurrentWeatherScreenState createState() => _CurrentWeatherScreenState();
}

class _CurrentWeatherScreenState extends State<CurrentWeatherScreen> {
  late WeatherCurrentCubit _cubit = BlocProvider.of<WeatherCurrentCubit>(context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: null,
        title: Container(
          width: 180,
          child: DropDownButton(),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/night_background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: BlocBuilder(
          bloc: _cubit,
          builder: (c, state) {
            if (state is LoadedModelState<WeatherResponse>) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  textInformation(state, state.model.name ?? "No name"),
                  textInformation(
                    state,
                    state.model.main?.pressure?.toString() ?? "No name",
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TemperatureTextWidget(
                      temp: state.model.main?.temp,
                      style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  textInformation(
                    state,
                    state.model.main?.temp?.toString() ?? "No name",
                  ),
                  if (state.model.weather.isNotEmpty)
                    textInformation(
                      state,
                      state.model.weather.first.main ?? "No name",
                    ),
                  textInformation(
                    state,
                    state.model.weather.first.id.toString(),
                  ),
                  textInformation(state, state.model.main?.getMessage ?? ""),
                ],
              );
            } else if (state is LoadingState) {
              return Center(
                child: Container(
                  height: 100,
                  width: 100,
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is ErrorState) {
              return Text(
                'Error - ${state.message}',
                style: TextStyle(
                  color: Colors.grey[900],
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              );
            } else {
              return Container(
                height: 200,
                width: 200,
                color: Colors.red,
              );
            }
          },
        ),
      ),
    );
  }

  Padding textInformation(LoadedModelState<WeatherResponse> state, text) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        text,
        style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class DropDownButton extends StatelessWidget {
  const DropDownButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        borderRadius: BorderRadius.circular(10),
        focusColor: Colors.white,
        dropdownColor: Colors.grey[300],
        elevation: 5,
        style: TextStyle(color: Colors.grey),
        iconEnabledColor: Colors.black,
        items: <String>[
          'Hourly Forecast',
          'Daily Forecast',
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: TextStyle(color: Colors.black),
            ),
          );
        }).toList(),
        hint: Text(
          'Select an action',
          style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w500),
        ),
        onChanged: (value) {
          if (value == 'Hourly Forecast') {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => WeatherHourlyCubit()..getData(),
                  child: HourlyWeatherScreen(),
                ),
              ),
            );
          }
          if (value == 'Daily Forecast') {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => WeatherDailyCubit()..getData(),
                  child: DailyWeatherScreen(),
                ),
              ),
            );
          }
        });
  }
}
