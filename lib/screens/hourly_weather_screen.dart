import 'package:weather_app/api/responses/weather_hourly_response.dart';
import 'package:weather_app/blocs/base_states.dart';
import 'package:weather_app/blocs/weather_hourly_cubit.dart';
import 'package:weather_app/helpers/format_date.dart';
import 'package:weather_app/screens/items/temperature_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HourlyWeatherScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WeatherHourlyCubit _cubit = BlocProvider.of<WeatherHourlyCubit>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Hourly Forecast',
          style: TextStyle(color: Colors.black, fontSize: 22),
        ),
        centerTitle: true,
        leading: Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/night_background.png'),
            fit: BoxFit.cover,
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: BlocBuilder(
          bloc: _cubit,
          builder: (context, state) {
            if (state is LoadedModelState<WeatherHourlyResponse> && state.model.hourly.isNotEmpty) {
              return ListView.separated(
                itemCount: state.model.hourly.length,
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemBuilder: (context, index) {
                  final model = state.model.hourly[index];

                  return InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("More info"),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      model.pressure != null ? "Pressure - ${model.pressure}" : "No data",
                                      style: TextStyle(),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      model.windSpeed != null ? "Wind Speed - ${model.windSpeed}" : "No data",
                                      style: TextStyle(),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  if (model.weather.isNotEmpty)
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        model.weather.first.description != null
                                            ? "${model.weather.first.description}"
                                            : "No data",
                                        style: TextStyle(),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          elevation: 5,
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  model.date != null ? formatDate(model.date, format: DateFormatType.Time) : "NaN",
                                  style: TextStyle(color: Colors.black),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TemperatureTextWidget(temp: model.temp),
                              ),
                              if (model.weather.isNotEmpty)
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    model.weather.first.getWeatherIcon,
                                    style: TextStyle(color: Colors.black),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ));
                },
              );
            } else if (state is LoadedModelState<WeatherHourlyResponse> && state.model.hourly.isEmpty) {
              return Text(
                'Emply list',
                style: TextStyle(
                  color: Colors.grey[900],
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              );
            } else if (state is LoadingState) {
              return Center(
                child: Container(
                  height: 100,
                  width: 100,
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is ErrorState) {
              return Text(
                'Error - ${state.message}',
                style: TextStyle(
                  color: Colors.grey[900],
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              );
            }

            return Container();
          },
        ),
      ),
    );
  }
}
