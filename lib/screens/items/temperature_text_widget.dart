import 'package:flutter/material.dart';

class TemperatureTextWidget extends StatelessWidget {
  final num? temp;
  final String? prefix;
  final TextStyle? style;

  const TemperatureTextWidget({
    Key? key,
    @required this.temp,
    this.prefix,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      temp != null ? "${prefix ?? ""} ${temp!.round()}\u2103" : "NaN",
      style: style == null ? TextStyle(color: Colors.black) : style,
      textAlign: TextAlign.center,
    );
  }
}

// ℃
// DEGREE CELSIUS
// Unicode: U+2103, UTF-8: E2 84 83
