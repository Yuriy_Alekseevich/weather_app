import 'package:intl/intl.dart';

enum DateFormatType {
  Date,
  Time,
  Day,
}

DateTime? stringToDate(String? date, {String? format}) {
  if (date != null) {
    if (format != null) {
      var inputFormat = DateFormat(format);
      return inputFormat.parse(date);
    } else {
      return DateTime.tryParse(date);
    }
  } else {
    return null;
  }
}

String formatDate(DateTime? date, {DateFormatType format = DateFormatType.Date}) {
  if (date != null) {
    switch (format) {
      case DateFormatType.Date:
        return DateFormat('dd.MM.yyyy').format(date.toLocal());
      case DateFormatType.Time:
        return DateFormat('hh:mm:ss').format(date.toLocal());
      case DateFormatType.Day:
        return DateFormat('MM.dd, EEEE').format(date.toLocal());
      default:
        return DateFormat('MM.dd.yyyy hh.mm.ss').format(date.toLocal());
    }
  } else {
    return "";
  }
}

DateTime? dateFromTimestamp(num? timestamp) {
  if (timestamp != null) {
    return DateTime.fromMillisecondsSinceEpoch(timestamp.toInt() * 1000).toLocal();
  }
  return null;
}
