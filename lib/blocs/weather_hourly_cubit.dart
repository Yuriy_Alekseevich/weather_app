import 'package:bloc/bloc.dart';
import 'package:weather_app/api/responses/weather_hourly_response.dart';
import 'package:weather_app/api/weather_api.dart';
import 'package:weather_app/blocs/base_states.dart';
import 'package:weather_app/services/location.dart';

import 'package:geolocator/geolocator.dart';

class WeatherHourlyCubit extends Cubit<BaseState> {
  final _api = WeatherApi();
  WeatherHourlyCubit() : super(InitialState());

  Future<void> getData() async {
    try {
      emit(LoadingState());

      Position position = await Location.loadCurrentLocation();
      var response = await _api.fetchHourlyWeather(position: position);

      if (response != null) {
        emit(LoadedModelState<WeatherHourlyResponse>(response));
      } else {
        emit(ErrorState(message: "Empty response"));
      }
    } catch (e) {
      emit(ErrorState(message: e.toString()));
    }
  }
}
