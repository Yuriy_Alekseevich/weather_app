import 'package:bloc/bloc.dart';
import 'package:weather_app/api/weather_api.dart';
import 'package:weather_app/blocs/base_states.dart';
import 'package:weather_app/services/location.dart';

import 'package:geolocator/geolocator.dart';

class WeatherCurrentCubit extends Cubit<BaseState> {
  final _api = WeatherApi();

  WeatherCurrentCubit() : super(InitialState());

  Future<void> getData() async {
    try {
      emit(LoadingState());

      Position position = await Location.loadCurrentLocation();
      var response = await _api.fetchCurrentWeather(position: position);
      if (response != null) {
        emit(LoadedModelState(response));
      }
    } catch (e) {
      emit(ErrorState(message: e.toString()));
    }
  }
}
