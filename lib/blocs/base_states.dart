import 'package:equatable/equatable.dart';

abstract class BaseState extends Equatable {
  const BaseState();
}

class InitialState extends BaseState {
  @override
  List<Object> get props => [];
}

class LoadingState extends BaseState {
  @override
  List<Object> get props => [];
}

class LoadedModelState<T extends Object> extends BaseState {
  LoadedModelState(this.model);
  final T model;
  @override
  List<Object> get props => [model];
}

class ErrorState extends BaseState {
  final String message;
  ErrorState({required this.message});
  @override
  List<Object> get props => [message];
}
